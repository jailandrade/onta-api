from graphql_geojson.filters import GeometryFilterSet
from .models import BusRoute


class BusRouteFilter(GeometryFilterSet):

    class Meta:
        model = BusRoute
        fields = {
            'name': ('exact', 'icontains'),
            'route': ('intersects', 'distance_lte'),
        }
