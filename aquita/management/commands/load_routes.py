from django.contrib.gis.utils import LayerMapping
from django.core.management.base import BaseCommand

import argparse

from aquita.models import BusRoute

attr_mapping = {
    'name': 'name',
    'desc': 'desc',
    'notes': 'notes',
    'route': 'LineString',
}


class Command(BaseCommand):
    help = 'Imports geojson files from a directory'

    def add_arguments(self, parser):
        parser.add_argument('filename', nargs='+', type=argparse.FileType('r'))

    def handle(self, *args, **options):
        for file_name in options['filename']:
            if not file_name.name.endswith('.geojson'):
                self.stdout.write(self.style.WARNING(
                    'Ignoring non-geojson file: {}'.format(file_name.name)
                ))
                continue

            lm = LayerMapping(BusRoute, file_name.name, attr_mapping,
                              transform=False)
            lm.save(strict=True, verbose=True)
