from django.contrib.gis.db import models
from django.contrib.gis.geos import Point
from django.conf import settings
from django.utils.translation import gettext_lazy as _


class BusTag(models.Model):
    name = models.CharField(max_length=50, verbose_name=_('bustag name'))

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('bus tag')
        verbose_name_plural = _('bus tags')


class BusRoute(models.Model):
    name = models.CharField(max_length=150, verbose_name=_('name'))
    desc = models.CharField(
        max_length=500, default=None, null=True, blank=True,
        verbose_name=_('description')
    )
    notes = models.CharField(
        max_length=500, default=None, null=True, blank=True,
        verbose_name=_('notes')
    )
    route = models.LineStringField(verbose_name=_('route'))
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    verified = models.BooleanField(default=False, verbose_name=_('verified'))
    company = models.CharField(
        max_length=150, null=True, default=None, blank=True,
        verbose_name=_('company')
    )
    color = models.CharField(
        max_length=50, null=True, default=None, blank=True,
    )
    tags = models.ManyToManyField(
        'BusTag', related_name='bus_routes',
        help_text=_("text on bus's front"), verbose_name=_('tags')
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('bus route')
        verbose_name_plural = _('bus routes')


class BusStop(models.Model):
    point = models.PointField(default=Point(**settings.DEFAULT_BUST_STOP),
                              verbose_name=_('punto'))
    name = models.CharField(
        max_length=150, default=None, null=True, blank=True,
        verbose_name=_('name')
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    osm_node_id = models.BigIntegerField(
        blank=True, default=None, null=True, unique=True,
        help_text=_("this bus stop has an OSM? This node has an ID?"),
        verbose_name=_('osm node id')
    )
    mapaton_id = models.BigIntegerField(
        blank=True, default=None, null=True, unique=True,
        help_text=_("Stop's id as defined in mapaton's file"),
        verbose_name=_('mapaton id')
    )
    source = models.CharField(max_length=20, choices=(
        ('osm', 'OSM'),
        ('mapaton', 'Mapatón'),
        ('hand', 'Creada a mano'),
    ), default='hand', verbose_name=_('source'))
    verified = models.BooleanField(default=False, verbose_name=_('verified'))

    def __str__(self):
        return str(self.point)

    class Meta:
        verbose_name = _('bus stop')
        verbose_name_plural = _('bus stops')
