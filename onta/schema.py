import graphene

import aquita.schema


class Query(aquita.schema.Query, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query)
